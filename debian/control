Source: dumpsterdiver
Section: utils
Priority: optional
Maintainer: Kali Developers <devel@kali.org>
Uploaders: Sophie Brun <sophie@offensive-security.com>
Build-Depends: debhelper-compat (= 13),
               dh-python,
               python3-all,
               python3-passwordmeter
Standards-Version: 4.6.1
Homepage: https://github.com/securing/DumpsterDiver
Vcs-Browser: https://gitlab.com/kalilinux/packages/dumpsterdiver
Vcs-Git: https://gitlab.com/kalilinux/packages/dumpsterdiver.git
Rules-Requires-Root: no

Package: dumpsterdiver
Architecture: all
Depends: python3-colorama,
         python3-passwordmeter,
         python3-termcolor,
         ${misc:Depends},
         ${python3:Depends}
Description: tool to analyze big volumes of data in search of hardcoded secrets
 This package contains a tool, which can analyze big volumes of data in search
 of hardcoded secrets like keys (e.g. AWS Access Key, Azure Share Key or SSH
 keys) or passwords. Additionally, it allows creating a simple search rules
 with basic conditions (e.g. report only csv files including at least 10 email
 addresses).
 .
 The main idea of this tool is to detect any potential secret leaks.
